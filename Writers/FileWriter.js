let Writer = require('./Writer')
let fs = require('fs')

class FileWriter extends Writer {
    constructor(config) {
        super(config)
    }

    write(data) {
        let d = `${this.config.fancy && this.config.fancy == true ?
            JSON.stringify(data, null, 1) :
            JSON.stringify(data)},\n`
        fs.appendFileSync(this.config.fileName, d, (err) => {
            if (err)
                console.error(err)
            console.log(d)
        })

    }
}
module.exports = FileWriter