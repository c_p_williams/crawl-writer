let Writer = require('./Writer')
let request = require('request')

class APIWriter extends Writer {
    constructor(config) {
        super(config)
    }

    write(data) {
        request.post({
            uri: this.config.url || this.config.uri,
            body: data,
            json: true,
            headers: this.config.headers || {}
        }, (err, resp, body) => {
            if (err)
                console.error(err)
            else
                console.log(body)
        })
    }
}

module.exports = APIWriter