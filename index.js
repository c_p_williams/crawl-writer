let writerTypes = require('./Writers/types')

class Writer {
    constructor(config) {
        if (Object.keys(writerTypes).includes(config.type)) {
            this.writer = new writerTypes[config.type](config)
        } else {
            throw new Error(`Invalid writer type - ${config.type}. Expected one of ${Object.keys(writerTypes)}`)
        }
    }

    write(data) {
        this.writer.write(data)
    }
}

module.exports = Writer