##Crawl Writer
[![Version](https://img.shields.io/npm/v/crawl-writer.svg)](https://npmjs.org/package/crawl-writer)
[![Build Status](https://img.shields.io/bitbucket/pipelines/c_p_williams/crawl-writer.svg)](https://bitbucket.org/c_p_williams/crawl-writer)
[![Downloads](https://img.shields.io/npm/dt/crawl-writer.svg)](https://npmjs.org/package/crawl-writer)

Simple module to write results of a [web-crawl](https://npmjs.org/package/web-crawl)